from .base import *

DEBUG = env.bool("DJANGO_DEBUG", default=True)

ALLOWED_HOSTS = ["localtest.me", "localhost", "127.0.0.1", "jps-vandelay.herokuapp.com"]


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        # 'NAME': 'vandelay3',
        # 'NAME': 'vandelay4',
        "NAME": "vandelay5",
        "USER": "joelsmith",
        "PASSWORD": "JeVousL0uvre",
        "HOST": "postgres",
        "PORT": "5432",
    }
}


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env(
    "DJANGO_SECRET_KEY", default="g7&2l3k)0$tr!=rgrefxo6om@tqz+@340-w*i5*2v(nzm_+zsd"
)
