from .base import *
# import django_heroku

ALLOWED_HOSTS = [
    '207.38.86.214',
    'jps-vandelay.herokuapp.com',
    '127.0.0.1',
    'localhost',
]

DEBUG = env.bool('DJANGO_DEBUG',False)

SECRET_KEY = env('DJANGO_SECRET_KEY')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'd6rmnrkpg0g36b',
        'USER': 'etlbacduzgmriz',
        'PASSWORD': '117b4017819d4ee0b2c21ddf65df7e617d4cb61f17166cb88242e9b9b2a67347',
        'HOST': 'ec2-52-73-247-67.compute-1.amazonaws.com',
        'PORT': '5432',
    }
}



# django_heroku.settings(locals())

# STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

AWS_S3_OBJECT_PARAMETERS = {
    'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
    'CacheControl': 'max-age=94608000',
}

AWS_QUERYSTRING_AUTH = False # This will make sure that the file URL does not have unnecessary parameters like your access key.

AWS_STORAGE_BUCKET_NAME = 'wwseatest'
AWS_S3_REGION_NAME = 'us-west-2'  # e.g. us-east-2
AWS_ACCESS_KEY_ID = 'AKIAZDIEXXC2JB42SQ72'
AWS_SECRET_ACCESS_KEY = 'Dbrq+7T9lQ+NN3on0EEe1Af2e9/WCsm6hxrYTwFz'

# Tell django-storages the domain to use to refer to static files.
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME

STATICFILES_LOCATION = 'static'
STATICFILES_STORAGE = 'custom_storages.StaticStorage'

MEDIAFILES_LOCATION = 'media'
DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'