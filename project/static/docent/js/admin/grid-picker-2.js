(function($) {
  
  'use strict';

  // placeholder for cached DOM elements
  var DOM = {};

  function cacheDom(){
    DOM.$gridPickers = $('.sortable-grid-picker');

    if(window.name == 'grid-picker-window'){
      DOM.$popupWindow = $(window);
    } else {
      DOM.$popupWindow = null;
    }
  }

  
  function bindEvents(){
    DOM.$gridPickers.on( 'sortstop', respondToSort );
    $('body').on( 'click', '.add-to-grid', maybeOpenPopup );
    $('a[data-popup-opener]').on( 'click', maybeClosePopup );
    if( DOM.$popupWindow ){
      $('#result_list tr a').on( 'click', selectContent );
    }
    
  }


  function selectContent(e){
    
    e.preventDefault();
    
    // read the content around this link to create an object
    var selection = parseSelectionInfo($(this));

    // close the window
    
    // using that object (CAN we still?), build a new grid cell
    // by cloning the empty one hanging off the end of the picker
    // (see line 61 in admin/inlines.js)
    
    // append the new grid cell
    // reorder everything (reOrderCells)
    console.log("Continue at selectContent");
  }


  function parseSelectionInfo( link ){
    var result = {};
    var row = $(link).closest('tr');
    result['id'] = $(row).find('.field-id').text();
    result['title'] = $(row).find('.field-title a').text();
    var img = $(row).find('.field-thumbnail img');
    if(img.length){
      result['img'] = img.attr('src');
    }
    return result;
  }

  
  function setUp(){
    
    // add a js class to the main container, for styling
    $('.jps-grid-picker-cell').addClass('js-enabled');

    // hide the Order and Image fields
    // $('.jps-grid-picker-cell .field-order').hide();
    // $('.jps-grid-picker-cell .field-image').hide();
    // $('.jps-grid-picker-container .add-row').remove();

    // initiate sortability (if the page has that library loaded)
    if ( typeof $('#jps-grid-picker-container').sortable == 'function' ) { 
      $('#jps-grid-picker-container').sortable();
    }

  }

  // generic handler for the end of a jquery sort
  function respondToSort( event, ui ){
    reOrderCells($(this));
  }

  // rewrites the ordering for all cells in a specific container
  function reOrderCells( container ){
    var cells = $(container).find('.jps-grid-picker-cell');
    $(cells).each(function(index){
      var orderBox = $(this).find('input[type="number"]');
      $(orderBox).val(index + 1);
    });
  }


  function maybeOpenPopup(e){
    e.preventDefault();
    var event = $.Event('django:lookup-related');
    $(this).trigger(event);
    if (!event.isDefaultPrevented()) {
        showRelatedObjectLookupPopup(this);
    }
  }


  function maybeClosePopup(e){
    console.log('testing');
    e.preventDefault();
    opener.dismissRelatedLookupPopup(window, $(this).data("popup-opener"));
  }

  
  function showRelatedObjectLookupPopup(triggeringLink) {
    return showAdminPopup(triggeringLink, /^lookup_/, true);
  }


  function showAdminPopup(triggeringLink, name_regexp, add_popup) {
    // var name = triggeringLink.id.replace(name_regexp, '');
    // name = id_to_windowname(name);
    var name = 'grid-picker-window';
    var href = triggeringLink.href;
    add_popup = false;
    if (add_popup) {
        if (href.indexOf('?') === -1) {
            href += '?_popup=1';
        } else {
            href += '&_popup=1';
        }
    }
    var win = window.open(href, name, 'height=500,width=800,resizable=yes,scrollbars=yes');
    DOM.$popupWindow = $(win);
    bindEvents();
    win.focus();
    return false;
  }

  $(document).ready(function() {

    cacheDom();
    bindEvents();
    setUp();

    $("a[data-popup-opener]").on('click', function(event) {
      event.preventDefault();
    });

  });

})(django.jQuery);