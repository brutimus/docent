# Generated by Django 2.2.5 on 2020-05-07 02:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0057_auto_20200506_1740'),
    ]

    operations = [
        migrations.CreateModel(
            name='SiteOption',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('opt_key', models.CharField(max_length=255, unique=True)),
                ('opt_value', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'site_options',
            },
        ),
    ]
