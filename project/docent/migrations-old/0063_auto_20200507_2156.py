# Generated by Django 2.2.5 on 2020-05-08 04:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0062_auto_20200507_2155'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='artwork',
            name='year',
        ),
        migrations.AddField(
            model_name='artwork',
            name='work_year',
            field=models.CharField(blank=True, default='', max_length=50, null=True),
        ),
    ]
