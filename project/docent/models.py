import math

from django.db import models
from django.db.models import Count, Max, Min
from django.utils.translation import ugettext_lazy as _
from django.dispatch import receiver
from datetime import datetime
from django.utils.dateparse import parse_datetime

from project.utils.models import CreationModificationDateMixin, PublishableMixin, Category
from project.utils.helpers import *

# image-related stuff
from versatileimagefield.fields import VersatileImageField, PPOIField
from versatileimagefield.image_warmer import VersatileImageFieldWarmer
from django.utils.html import mark_safe
import requests

class PublishedObjectsManager(models.Manager):
    def get_queryset(self):
        now = datetime.now()
        return (
            super(NotificationManager, self)
            .get_queryset()
            .filter(
                post_status='published'
            )
        )

class Image(CreationModificationDateMixin):

    legacy_id = models.BigIntegerField(blank=True,null=True,unique=True)
    title = models.CharField(max_length=200)
    slug = models.SlugField( unique=True, max_length=200 )
    alt = models.CharField(max_length=400, blank=True)
    caption = models.CharField(max_length=255, blank=True)
    should_fetch_ar = models.BooleanField(default=0,verbose_name=_("Generate AR model?"))
    ar_ios = models.URLField(blank=True,null=True)
    ar_android = models.URLField(blank=True,null=True)
    ww_img = models.URLField(max_length=500,blank=True,null=True)
    img = VersatileImageField(
        'Image',
        upload_to='images/',
        ppoi_field='img_ppoi',
        blank=True,
        null=True
    )
    img_ppoi = PPOIField()

    class Meta:
        db_table = "ww_images"

    def __str__(self):
        return self.title

    def table_thumbnail(self):
        if self.img:
            print( self.img.crop['350x350'] )
            return mark_safe('<img src="%s" width="100" height="100" />' % (self.img.crop['350x350']))
        else:
            return ''

    def save(self, *args, **kwargs):
        slug_str = "%s" % (self.title) 
        unique_slugify(self, slug_str) 
        abracadabra = self.maybe_fetch_AR()
        if abracadabra:
            self.ar_ios = abracadabra['iosURL']
            self.ar_android = abracadabra['androidURL']
            self.should_fetch_ar = False
        super(Image, self).save(*args, **kwargs)

    def maybe_fetch_AR(self):

        # bail if we don't need to fetch the AR
        if not self.should_fetch_ar: return False

        # bail if the image doesn't actually have an image
        if not self.ww_img: return False

        return self.fetch_AR()

    def fetch_AR(self):
        
        houdini_url = "https://virtserver.swaggerhub.com/Houdini_Interactive/Winston-AR-API/0.0.1/generate"
        
        headers = {
            'Content-Type': 'text/plain',
            'API_KEY': 'password'
        }

        payload = {
            'id': str(self.id),
            'imageURL': self.ww_img,
            'size': {
                'width': self.img.width,
                'height': self.img.height
            },
        }
        
        r = requests.post(
            houdini_url, 
            headers = headers,
            data = payload
        )
        
        if r.status_code == 200:
            r.encoding = 'utf-8'
            return r.json()
        else:
            return False
        
        # return self.handle_AR_success()
    
    # def handle_AR(self):
        # self.handle_AR_success()

    def handle_AR_success(self):
        print("Let's pretend we succeeded")
        result = {
            'ar_ios': 'https://www.houdini.studio/',
            'ar_android': 'https://www.houdini.studio/',
        }
        return result

@receiver(models.signals.post_save, sender=Image)
def warm_Image_crops(sender, instance, **kwargs):
    if instance.img:
        img_warmer = VersatileImageFieldWarmer(
            instance_or_queryset=instance,
            rendition_key_set='standard_crops', # see settings.VERSATILEIMAGEFIELD_RENDITION_KEY_SETS
            image_attr='img' # this matches the name of the attribute on the model
        )
        instance.img.delete_sized_images()
        num_created, failed_to_create = img_warmer.warm()

class Video(PublishableMixin):

    legacy_id = models.BigIntegerField(blank=True,null=True,unique=True)
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True,default='')
    video_url = models.URLField(blank=True,null=True)

    class Meta:
        db_table = "ww_videos"

class Post(PublishableMixin):

    legacy_id = models.BigIntegerField(blank=True,null=True,unique=True)
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True,default='')
    artists = models.ManyToManyField( 'Artist', blank=True, verbose_name=_('Artists') )
    exhibitions = models.ManyToManyField( 'Exhibition', blank=True, verbose_name=_('Exhibitions') )
    categories = models.ManyToManyField( 'PostCategory', blank=True )

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.created_at <= now

    class Meta:
        db_table = "ww_posts"

class PostCategory(Category):
    
    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

class Artist(PublishableMixin):

    ACTIVE_STATUSES = (
        ('active',_('Active')),
        ('inactive',_('Inactive')),
    )

    legacy_id = models.BigIntegerField(blank=True,null=True,unique=True)
    title = models.CharField(max_length=200)
    first_name = models.CharField(blank=True,default='',max_length=200)
    last_name = models.CharField(default='',max_length=200)
    description = models.TextField(blank=True,default='')
    active_status = models.CharField(max_length=10, choices=ACTIVE_STATUSES, default='inactive',verbose_name=_('Status'))
    available = models.BooleanField(default=1,verbose_name=_('Currently available?'))
    featured_image = models.ForeignKey(Image, blank=True, null=True, on_delete=models.SET_NULL, related_name='artist_featured_image' )
    work_images = models.ManyToManyField(Image, through='ArtistImage', blank=True, verbose_name=_('Work Image(s)'), related_name='artist_images')
    install_images = models.ManyToManyField(Image, through='ArtistInstallImage', blank=True, verbose_name=_('Installation Image(s)'), related_name='artist_install_images')
    press_clips = models.ManyToManyField('Document', blank=True, verbose_name=_('Press Clip(s)'))
    bio_pdf = models.ForeignKey('Document', blank=True, null=True, on_delete=models.SET_NULL, related_name='artist_bio_pdf' )

    class Meta:
        db_table = "ww_artists"
        ordering = ['last_name',]

    def __str__(self):
        return self.title

    def raw_name(self):
        result = ""
        if self.first_name: result += self.first_name + " "
        if self.last_name: result += self.last_name
        return result.strip()

    def display_name(self):
        result = ""
        if self.last_name: result += "<b>" + self.last_name + "</b>"
        if self.first_name: result += ", " + self.first_name
        return result.strip()

    def get_works(self, exclude_id = None):

        results = self.artwork_set.all()

        if not results:
            return None

        if exclude_id:
            results = results.exclude( id=exclude_id )

        if not results:
            return None

        return results[:14]

class PressClipping(CreationModificationDateMixin):

    title = models.CharField(max_length=200)
    publication_name = models.CharField(max_length=200,default='')
    url = models.URLField(blank=True,null=True)
    file_fallback = models.ForeignKey('Document', blank=True, null=True, on_delete=models.SET_NULL)

class ArtistImage(models.Model):
    artist = models.ForeignKey('Artist', on_delete=models.CASCADE)
    image = models.ForeignKey('Image', on_delete=models.CASCADE)
    order = models.IntegerField()

    def __str__(self):
        return self.image.title

    class Meta():
        ordering = ['order',]

class ArtistInstallImage(models.Model):
    artist = models.ForeignKey('Artist', on_delete=models.CASCADE)
    image = models.ForeignKey('Image', on_delete=models.CASCADE)
    order = models.IntegerField()

    def __str__(self):
        return self.image.title

    class Meta():
        ordering = ['order',]

class Exhibition(PublishableMixin):

    title = models.CharField(max_length=200)
    description = models.TextField(blank=True,default='')
    legacy_id = models.BigIntegerField(blank=True,null=True,unique=True)
    open_date = models.DateField(blank=True,null=True)
    close_date = models.DateField(blank=True,null=True)
    reception = models.CharField(blank=True,default='',max_length=200)
    artist_in_attendance = models.BooleanField(default=0,verbose_name=_("Artist in attendance?"))
    artists = models.ManyToManyField(Artist,blank=True)
    featured_image = models.ForeignKey('Image', blank=True, null=True, on_delete=models.SET_NULL, related_name='exhibition_featured_image' )
    images = models.ManyToManyField('Image', through='ExhibitionImage', blank=True, verbose_name=_('Image(s)'))

    class Meta:
        db_table = "ww_exhibitions"

    def __str__(self):
        return self.title

    def get_posts_per_page():
        exhibitions_per_page = SiteOption.get_option('exhibitions_per_page')
        if exhibitions_per_page: 
            return exhibitions_per_page
        posts_per_page = SiteOption.get_option('posts_per_page')
        if posts_per_page: 
            return posts_per_page
        else:
            return 5

    def get_past():
        now = datetime.now()
        return Exhibition.objects.filter( close_date__lte=now, post_status='published' ).order_by( '-close_date' )

    def get_current():
        now = datetime.now()
        return Exhibition.objects.filter( open_date__lte=now, close_date__gte=now, post_status='published' ).order_by('-close_date')

    def get_upcoming():
        now = datetime.now()
        return Exhibition.objects.filter( open_date__gt=now, post_status='published' ).order_by('-close_date')

    @property
    def display_artists(self):

        artists = self.artists.all()
        if not artists: return None
        display = ''
        num_artists = artists.count()
        if num_artists == 1:
            display = artists[0].title
        elif num_artists == 2:
            display = artists[0].title + " & " + artists[1].title
        else:
            for artist in artists:
                display = display + artist.title + ', '
            display = display.rstrip(', ')
        return display

    @property
    def display_dates(self):
        
        result = ''
        if not self.open_date or not self.close_date:
            return result
        
        opening_year = self.open_date.year
        closing_year = self.close_date.year
        from_format = "%B %-d, %Y"

        # sort out the format, depending if the exhibition spans calendar years
        if opening_year == closing_year:
            to_format = "%B %-d"
        else:
            to_format = from_format

        # assemble and return the string
        result = self.open_date.strftime( to_format ) + ' - ' + self.close_date.strftime( from_format )
        return result



class ExhibitionImage(models.Model):
    exhibition = models.ForeignKey('Exhibition', on_delete=models.CASCADE)
    image = models.ForeignKey('Image', on_delete=models.CASCADE)
    order = models.IntegerField()

    def __str__(self):
        return self.image.title

    class Meta():
        ordering = ['order',]

class ArtworkQuerySet(models.QuerySet):

    # https://wellfire.co/learn/simple-search-manager-methods/

    def search(self,**kwargs):
        return self.filter(post_status='published',searchable=True,variations__isnull=False)

class Artwork(PublishableMixin):

    legacy_id = models.PositiveIntegerField(blank=True,null=True,unique=True)
    title = models.CharField(max_length=200)
    slug = models.SlugField( unique=True, blank=True, max_length=200 )
    description = models.TextField(blank=True,default='')
    year = models.CharField( max_length=50, blank=True, default='' )
    fm_id = models.CharField(max_length=50,blank=True,default='',verbose_name=_('FileMaker Id'))
    date_acquired = models.DateField(blank=True,null=True)
    condition = models.CharField(max_length=200,blank=True,default='')
    admin_notes = models.TextField(blank=True,default='')
    work_mediums = models.ManyToManyField( 'ArtworkMedium', blank=True, related_name='mediums', verbose_name=_('Work Medium(s)') )
    work_aspects = models.ManyToManyField( 'ArtworkAspect', blank=True, related_name='aspects', verbose_name=_('Work Aspect(s)') )
    work_types = models.ManyToManyField( 'ArtworkType', blank=True, related_name='types', verbose_name=_('Work Type(s)') )
    location = models.ForeignKey( 'ArtworkLocation', blank=True, null=True, related_name='location', verbose_name=_('Work Location'), on_delete=models.SET_NULL )
    work_images = models.ManyToManyField('Image', through='ArtworkImage', blank=True, verbose_name=_('Work Image(s)'), related_name='artwork_images')
    featured_image = models.ForeignKey('Image', blank=True, null=True, related_name='artwork_featured_image', on_delete=models.SET_NULL )
    artist = models.ForeignKey('Artist', blank=True, null=True, on_delete=models.SET_NULL )
    signature_status = models.ForeignKey( 'ArtworkSignatureStatus', blank=True, null=True, related_name='signature_status', verbose_name=_('Signature Status'), on_delete=models.SET_NULL )
    signature_notes = models.CharField(max_length=200,blank=True,default='')
    searchable = models.BooleanField(default=1,verbose_name=_("Is this a searchable work?"))
    objects = models.Manager()
    search_objects = ArtworkQuerySet.as_manager()

    class Meta:
        db_table = "ww_artworks"

    def __str__(self):
        return self.title

    def save(self, **kwargs):
        slug_str = "%s" % (self.title) 
        unique_slugify(self, slug_str) 
        super(Artwork, self).save(**kwargs)

    def featured_image_thumbnail(self):
        if self.featured_image and self.featured_image.img:
            return mark_safe('<img src="%s" width="100" height="100" />' % (self.featured_image.img.crop['350x350']))
        else:
            return ''

    def search_collection(get):
        return Artwork.objects.filter( searchable=True )[:15]

    def get_details_list(self):
        
        result = {}
        
        if self.work_mediums.exists():
            mediums = ''
            for m in self.work_mediums.all():
                mediums = mediums + m.title + ', '
            result['mediums'] = mediums.rstrip(', ')

        if self.variations.exists():
            variations = []
            num_variations = self.variations.count()
            for v in self.variations.all():
                height = str(v.height) + " in x "
                width = str(v.width) + " in"
                dimensions = height + width + " x " + str(v.depth) if v.depth else height + width    
                if num_variations > 1: 
                    price = " - $" + str(int(v.total_price)) if v.show_price else ""
                    string_result = dimensions + price
                    variations.append(string_result)
                else:
                    result['size'] = dimensions
                    result['price'] = "$" + str(int(v.total_price))
            result['variations'] = variations
        
        return result

    def get_search_filters(get):

        results = {}
        results['price_min'] = Artwork.get_value_limits( get, 'min', 'total_price', 'price_min', 'dollars' )
        results['price_max'] = Artwork.get_value_limits( get, 'max', 'total_price', 'price_max', 'dollars' )
        results['height_min'] = Artwork.get_value_limits( get, 'min', 'height', 'height_min', 'inches' )
        results['height_max'] = Artwork.get_value_limits( get, 'max', 'height', 'height_max', 'inches' )
        results['width_min'] = Artwork.get_value_limits( get, 'min', 'width', 'width_min', 'inches' )
        results['width_max'] = Artwork.get_value_limits( get, 'max', 'width', 'width_max', 'inches' )
        return results


    def get_value_limits( data, min_max = 'min', field = 'total_price', parameter = 'price_min', units = 'dollars' ):

        # use bleach to sanitize text inputs: https://github.com/mozilla/bleach

        if min_max == 'min':
            val = ArtworkVariation.objects.aggregate(Min(field))
        else:
            min_max = 'max'
            val = ArtworkVariation.objects.aggregate(Max(field))

        val = math.ceil(val[f"{field}__max"]) if min_max == 'max' else math.floor(val[f"{field}__min"])

        # set up default value for results
        results = {
            'limit': {
                'raw': 0.00,
                'string': '0'
            },
            'selected': False
        }

        results['limit']['raw'] = val
        results['limit']['string'] = format_inches(val) if units == 'inches' else convert_to_dollars(val)

        if parameter in data:
            val = float(data[parameter]) if parameter == 'height' or parameter == 'width' else int(data[parameter])
            val = math.ceil(val) if min_max == 'max' else math.floor(val)
            selection = {
                'raw': val,
                'string': format_inches(val) if units == 'inches' else convert_to_dollars(val)
            }
            results['selected'] = selection

        return results
            

class ArtworkVariation(CreationModificationDateMixin):

    class Meta:
        db_table = 'art_variations'
        ordering = ['total_price']

    legacy_id = models.BigIntegerField(blank=True,null=True,unique=True)
    artwork = models.ForeignKey( 'Artwork', on_delete=models.CASCADE, related_name='variations' )
    height = models.DecimalField(max_digits=10, decimal_places=2)
    width = models.DecimalField(max_digits=10, decimal_places=2)
    depth = models.DecimalField(max_digits=10, decimal_places=2)
    display_size = models.CharField( max_length=100, blank=True, null=True )
    base_price = models.DecimalField( max_digits=10, decimal_places=2, blank=True, null=True )
    framing_price = models.DecimalField( max_digits=10, decimal_places=2, blank=True, null=True )
    production_price = models.DecimalField( max_digits=10, decimal_places=2, blank=True, null=True )
    total_price = models.DecimalField( max_digits=10, decimal_places=2, blank=True, null=True )
    num_editions = models.PositiveSmallIntegerField( default=0, blank=True, null=True, verbose_name=_('Editions Available') )
    max_editions = models.PositiveSmallIntegerField( default=1000, blank=True, null=True, verbose_name=_('Editions Allowed') )
    proofs = models.PositiveSmallIntegerField( default=0, blank=True, null=True )
    show_price = models.BooleanField(default=1,verbose_name=_("Show price?"))

    def __str__(self):
        return str(self.id)

class ArtworkImage(models.Model):
    artwork = models.ForeignKey('Artwork', on_delete=models.PROTECT)
    image = models.ForeignKey('Image', on_delete=models.PROTECT)
    order = models.IntegerField()

    def __str__(self):
        return self.image.title

    class Meta():
        ordering = ['order',]

class ArtworkMedium(Category):
    
    class Meta:
        verbose_name = _('Medium')
        verbose_name_plural = _('Mediums')

class ArtworkAspect(Category):

    class Meta:
        verbose_name = _('Visual Aspect')
        verbose_name_plural = _('Visual Aspects')

class ArtworkType(Category):

    class Meta:
        verbose_name = _('Type')
        verbose_name_plural = _('Types')

class ArtworkLocation(Category):

    class Meta:
        verbose_name = _('Location')
        verbose_name_plural = _('Locations')

class ArtworkSignatureStatus(Category):

    class Meta:
        verbose_name = _('Signature Status')
        verbose_name_plural = _('Signature Statuses')

class Contact(CreationModificationDateMixin):

    legacy_id = models.BigIntegerField(blank=True,null=True,unique=True)
    first_name = models.CharField(blank=True,default='',max_length=200)
    last_name = models.CharField(blank=True,default='',max_length=200)
    position = models.CharField( max_length=200, blank=True, default='' )
    salutation = models.CharField( max_length=200, blank=True, default='' )
    company = models.CharField( max_length=200, blank=True, default='' )
    admin_notes = models.TextField(blank=True,default='')
    artists = models.ManyToManyField(Artist,blank=True)
    categories = models.ManyToManyField( 'ContactCategory', blank=True, related_name='categories', verbose_name=_('Categories') )
    fax = models.CharField(blank=True,default='',max_length=50)

    class Meta():
        ordering = ['last_name','first_name','company',]

    def __str__(self):
        return self.record_name()

    def record_name(self):
        if not self.first_name and not self.last_name and self.company: return self.company
        elif self.first_name and self.last_name: return self.last_name + ", " + self.first_name
        elif self.last_name: return self.last_name
        elif self.first_name: return self.first_name
        else: return ''

class Address(CreationModificationDateMixin):

    ADDRESS_TYPES = (
        ('work',_('Work')),
        ('home',_('Home')),
        ('other',_('Other')),
    )
    
    legacy_id = models.BigIntegerField(blank=True,null=True,unique=True)
    contact = models.ForeignKey( 'Contact', blank=True, null=True, related_name='addresses', verbose_name=_('Contact'), on_delete=models.SET_NULL )
    address_1 = models.CharField(blank=True,default='',max_length=200)
    address_2 = models.CharField(blank=True,default='',max_length=200)
    city = models.CharField(blank=True,default='',max_length=75)
    state = models.CharField(blank=True,default='',max_length=50)
    zipcode = models.CharField(blank=True,default='',max_length=50)
    country = models.CharField(blank=True,default='',max_length=50)
    type = models.CharField(max_length=10, choices=ADDRESS_TYPES, default='work',verbose_name=_('Address Type'))
    primary = models.BooleanField(default=0,verbose_name=_("Primary address?"))
    notes = models.CharField(blank=True,default='',max_length=100)

class Telephone(CreationModificationDateMixin):

    NUMBER_TYPES = (
        ('work',_('Work')),
        ('home',_('Home')),
        ('other',_('Other')),
    )

    contact = models.ForeignKey( 'Contact', blank=True, null=True, related_name='telephones', verbose_name=_('Phone Number'), on_delete=models.SET_NULL )
    number = models.CharField(blank=True,default='',max_length=50)
    type = models.CharField(max_length=10, choices=NUMBER_TYPES, default='work',verbose_name=_('Number Type'))
    primary = models.BooleanField(default=0,verbose_name=_("Primary number?"))
    note = models.CharField(blank=True,default='',max_length=100)

    def __str__(self):
        return self.number

class ContactCategory(Category):

    class Meta:
        verbose_name = _('Contact Category')
        verbose_name_plural = _('Contact Categories')

class Email(CreationModificationDateMixin):

    contact = models.ForeignKey( 'Contact', blank=True, null=True, related_name='emails', verbose_name=_('Email Address'), on_delete=models.SET_NULL )
    address = models.CharField(blank=True,default='',max_length=100)
    primary = models.BooleanField(default=0,verbose_name=_("Primary address?"))
    note = models.CharField(blank=True,default='',max_length=100)
    private = models.BooleanField(default=0,verbose_name=_("Private?"))
    lists = models.ManyToManyField( 'EmailList', blank=True, related_name='lists', verbose_name=_('Email List(s)') )

    def __str__(self):
        return self.address

class EmailList(CreationModificationDateMixin):

    class Meta:
        verbose_name = _('Email List')
        verbose_name_plural = _('Email Lists')

    title = models.CharField(max_length=200)
    description = models.TextField(blank=True,default='')
    legacy_id = models.BigIntegerField(blank=True,null=True,unique=True)

class DemoCollection(CreationModificationDateMixin):

    title = models.CharField(max_length=200)
    description = models.TextField(blank=True,default='')
    admin_notes = models.TextField(blank=True,default='')
    artworks = models.ManyToManyField(Artwork, through='DemoCollectionWork', blank=True, verbose_name=_('Image(s)'), related_name='demo_collection_works')

    class Meta:
        verbose_name = _('Demo Collection')
        verbose_name_plural = _('Demo Collections')

    def __str__(self):
        return self.title

class DemoCollectionWork(models.Model):
    collection = models.ForeignKey(DemoCollection, on_delete=models.CASCADE)
    artwork = models.ForeignKey(Artwork, on_delete=models.CASCADE)
    order = models.IntegerField()

    def __str__(self):
        return self.artwork.title

    class Meta():
        ordering = ['order',]

class SiteOption(models.Model):

    class Meta:
        db_table = "site_options"

    opt_key = models.CharField(max_length=255,unique=True)
    opt_value = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_option( key, type = int ):
        try:
            raw = SiteOption.objects.only('opt_value').get(opt_key=key)
            if type is int: return int(raw.opt_value)
            else: return raw.opt_value
        except:
            return None

class Consignment(CreationModificationDateMixin):

    legacy_id = models.BigIntegerField(blank=True,null=True,unique=True)
    title = models.CharField(max_length=200)
    commission = models.CharField( max_length=200, blank=True, default='' )
    admin_notes = models.TextField(blank=True,default='')
    terms = models.TextField(blank=True,default='')
    accounting_notes = models.TextField(blank=True,default='')
    client = models.ForeignKey(Contact, blank=True, null=True, on_delete=models.SET_NULL, related_name='consignment' )
    status = models.ForeignKey( 'ConsignmentStatus', blank=True, null=True, related_name='consignment_status', on_delete=models.SET_NULL )
    artworks = models.ManyToManyField(Artwork, through='ConsignmentWork', blank=True, verbose_name=_('Artwork(s)'))
    total_value = models.DecimalField( max_digits=10, decimal_places=2, default=0 )
    documents = models.ManyToManyField('Document', blank=True, verbose_name=_('Supporting Document(s)'))

class ConsignmentStatus(Category):

    class Meta:
        verbose_name = _('Consignment Status')
        verbose_name_plural = _('Consignment Statuses')

class ConsignmentWork(models.Model):
    consignment = models.ForeignKey(Consignment, on_delete=models.CASCADE)
    artwork = models.ForeignKey(Artwork, on_delete=models.CASCADE)
    order = models.IntegerField()
    line_item_value = models.DecimalField( max_digits=10, decimal_places=2, default=0 )

    def __str__(self):
        return self.artwork.title

    class Meta():
        ordering = ['order',]

class Invoice(CreationModificationDateMixin):

    fm_id = models.CharField(max_length=50,blank=True,default='',verbose_name=_('FileMaker Id'))
    snipcart_id = models.CharField(max_length=50,blank=True,default='',verbose_name=_('Snipcart Id'))
    client = models.ForeignKey(Contact, blank=True, null=True, on_delete=models.SET_NULL)
    total = models.DecimalField( max_digits=10, decimal_places=2, default=0 )
    admin_notes = models.TextField(blank=True,default='')

class Moment(CreationModificationDateMixin):
    
    description = models.TextField(blank=True,default='')
    artist = models.ForeignKey(Artist, blank=True, null=True, on_delete=models.SET_NULL, related_name='history' )
    exhibition = models.ForeignKey(Exhibition, blank=True, null=True, on_delete=models.SET_NULL, related_name='history' )
    artwork = models.ForeignKey(Artwork, blank=True, null=True, on_delete=models.SET_NULL, related_name='history' )
    contact = models.ForeignKey(Contact, blank=True, null=True, on_delete=models.SET_NULL, related_name='history' )
    demo_collection = models.ForeignKey(DemoCollection, blank=True, null=True, on_delete=models.SET_NULL, related_name='history' )
    invoice = models.ForeignKey(Invoice, blank=True, null=True, on_delete=models.SET_NULL, related_name='history' )
    consignment = models.ForeignKey(Consignment, blank=True, null=True, on_delete=models.SET_NULL, related_name='history' )

    class Meta():
        ordering = ['-created_at',]

class Document(CreationModificationDateMixin):

    title = models.CharField(max_length=200)
    slug = models.SlugField( unique=True, max_length=200 )
    description = models.TextField(blank=True)
    document_file = models.FileField(upload_to='documents/%Y/%m/')

    def __str__(self):
        return '%s' % self.title

    @property
    def url(self):
        return self.document_file
