from django.urls import path, include
from . import views

app_name = "docent"
urlpatterns = [
    path("", views.home, name="home"),
    path("exhibitions/", views.exhibition_index, name="exhibition_index"),
    path(
        "exhibitions/page/<int:page>", views.exhibition_index, name="exhibition_index"
    ),
    path("exhibitions/<slug:slug>/", views.exhibition_detail, name="exhibition_detail"),
    path("artists/", views.artist_index, name="artist_index"),
    path("artists/<slug:slug>/", views.artist_detail, name="artist_detail"),
    path("news/", views.news_index, name="news_index"),
    path("news/<slug:slug>/", views.news_detail, name="news_detail"),
    path("search-collection/", views.search_collection, name="search_collection"),
    path("collection/<slug:slug>/", views.artwork_detail, name="artwork_detail"),
    # path('myadmin/gridpicker', views.GridPicker.as_view(), name='gridpicker'),
    # path('myadmin/', admin_site.urls),
    # path('artists/<slug:slug>/', views.artist_detail, name='artist_detail'),
    # path('artists/', views.artist_index, name='artist_index'),
    # path('search/', views.search_collection, name='search_collection'),
    # path('work/<slug:slug>/', views.work_detail, name='work_detail'),
    # path('pdf/', views.print_pdf, name='print_pdf'),
    # path('image-autocomplete/', views.ImageAutocomplete.as_view(), name='image_autocomplete'),
]
