from django.db import models
from django.utils.translation import ugettext_lazy as _
# from treebeard.mp_tree import MP_Node

class CreationModificationDateMixin(models.Model):
    """
    Abstract base class with a creation
    and modification date and time
    """
    class Meta:
        abstract = True
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class PublishableMixin(CreationModificationDateMixin):

    POST_STATUSES = (
        ('draft',_('Draft')),
        ('published',_('Published')),
    )

    slug = models.SlugField( unique=True, max_length=200 )
    post_status = models.CharField(max_length=12, choices=POST_STATUSES, default='draft',verbose_name=_('Post Status'))

    class Meta:
        abstract = True

class Category(models.Model):
    title = models.CharField(
        max_length=200,
        help_text=_("Short descriptive name for this category."),
    )
    slug = models.SlugField(
        max_length=255,
        db_index=True,
        unique=True,
        help_text=_("Short descriptive unique name for use in urls."),
    )
    legacy_id = models.PositiveIntegerField(
        blank=True,
        null=True,
        unique=True
    )
    def __unicode__(self):
        return self.title

    __str__ = __unicode__

    class Meta:
        abstract = True
        ordering = ("title",)
        verbose_name = _("category")
        verbose_name_plural = _("categories")

    def get_absolute_url(self):
        return reverse("category_object_list", kwargs={"category_slug": self.slug})